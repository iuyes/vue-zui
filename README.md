# vui

## 移动端UI框架，基于Vue.js实现。



### vui 组件库

button

![输入图片说明](http://git.oschina.net/uploads/images/2016/0725/140938_9ece587f_331468.png "在这里输入图片标题")

radio

![输入图片说明](http://git.oschina.net/uploads/images/2016/0722/175738_d5373667_331468.png "在这里输入图片标题")

radioGroup

![输入图片说明](http://git.oschina.net/uploads/images/2016/0725/141024_ae1ef885_331468.png "在这里输入图片标题")

checkbox

![输入图片说明](http://git.oschina.net/uploads/images/2016/0722/175757_c890aa46_331468.png "在这里输入图片标题")

checkboxGroup

![输入图片说明](http://git.oschina.net/uploads/images/2016/0725/141051_5b3b7b20_331468.png "在这里输入图片标题")

**Toast(new)**

![输入图片说明](http://git.oschina.net/uploads/images/2016/0722/175816_87661742_331468.png "在这里输入图片标题")

**MessageActions(new)**

![输入图片说明](http://git.oschina.net/uploads/images/2016/0722/175838_80ffec4a_331468.png "在这里输入图片标题")

MessageAlert

![输入图片说明](http://git.oschina.net/uploads/images/2016/0722/175850_5fed4f0a_331468.png "在这里输入图片标题")

MessageConfirm

![输入图片说明](http://git.oschina.net/uploads/images/2016/0722/175903_b2d72c9a_331468.png "在这里输入图片标题")

MessageLoading

![输入图片说明](http://git.oschina.net/uploads/images/2016/0722/175916_dd9d4a70_331468.png "在这里输入图片标题")

Switch

![输入图片说明](http://git.oschina.net/uploads/images/2016/0722/175930_2243f386_331468.png "在这里输入图片标题")

**DatePicker(new)**

![输入图片说明](http://git.oschina.net/uploads/images/2016/0722/175947_c0fa22c9_331468.png "在这里输入图片标题")

**CitySelect(new)**

![输入图片说明](http://git.oschina.net/uploads/images/2016/0722/180002_f8f210ee_331468.png "在这里输入图片标题")

**Calendar(new)**

![输入图片说明](http://git.oschina.net/uploads/images/2016/0722/180020_f1cdd48c_331468.png "在这里输入图片标题")

**Validator(new)**

![输入图片说明](http://git.oschina.net/uploads/images/2016/0722/180038_1b227343_331468.png "在这里输入图片标题")

### vui待开发组件

table

list

refresh

swiper

slider

upload

popup

alert

tip



## Browser
	
        <link href="../dist/vui.min.css" rel="stylesheet">
        <script src="../dist/vue.js"></script>
        <script src="../dist/vui.min.js"></script>
        <script>
        var Toast= vui.Toast
        var app = new Vue({
            el : '#app',
            components:{
 		Toast
 	    }
        });
        </script>

## CommonJS
        
        

	<city-select :visible="isCityVisible" @change="changeCity" @close="closeCity"></city-select>

	import citySelect from "vue-zui/src/components/select/citySelect/"
	
	new Vue({
		el:"#app",
		components:{
			citySelect:citySelect
		}
	});



## nodeJs

npm install vue-zui





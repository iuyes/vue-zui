var rulesMap=new Object();

var ruleUtil={
	add:function(name,func){
		rulesMap[name]=func;
	},
	remove:function(name){
		delete rulesMap[name]
	}
};

/**
 * [require description]
 * @authors zhanwang.ye
 * @param  {[type]} value [description]
 * @return {[type]}       [description]
 */
ruleUtil.add("required",function(value){
	if(typeof value!="undefined"&&value.toString()!=""){
		return true;
	}
	else{
		return false;
	}
});


ruleUtil.add("mobile",function(value){
	var reg=/^1\d{10}$/i;
	
	if(typeof value=="undefined"&&value.toString()==""){
		return false;
	}
	else{
		if(reg.test(value)){
			return true;
		}
	}
	return false;
});

module.exports=rulesMap;